# Family-Weather-Station

Family-Weather-Station

------

`[type:feat] task #9999 body`

commit信息包括三个字段：type（必需），scope（可选）和subject（必需）

1. type：用于说明该commit的类型的
   - feat：新功能（feature）【初始化项目】【新增业务功能】
   - docs：文档（documents）【增加注释】
   - chore：构建或辅助工具的变动【删除注释/空白行】
   - refactor: 重构【抽取通用方法】【修改变量名/类型】
   - revert: 撤销上一次提交
   - fix：修复bug【修复xx问题】【修改配置文件】
   - style：代码格式（不影响代码运行的格式变动，注意不是指CSS的修改
   - test：提交测试代码（单元测试，集成测试等）
   - misc：一些未归类或不知道将它归类到什么方面的提交

2. scope：用于说明此次代码提交影响的范围，这个视项目不同而定一般可以按照功能模块来对应

3. subject：是对于该commit目的的简短描述，body其实就是subject的详细说明

4. ISSUEE-ID：bug序号。当修改bug提交时，必须录入bug序号