#include <Arduino.h>

/**
 * @brief 开发板程序启动，点亮板载LED
 * 
 * @return void
 */
void board_start()
{
    pinMode(48, OUTPUT);
    for (int i = 0; i < 3; i++)   // 板载LED闪烁3次
    {
        digitalWrite(48, LOW);
        delay(100);
        digitalWrite(48, HIGH);
        delay(100);
        
    }
    Serial.begin(115200); // 串口初始化
    Serial.println("[ESP32S3] -> board is ready"); // 打印提示信息
    Serial.flush(); // 清空缓冲区
    Serial.end(); // 关闭串口

}