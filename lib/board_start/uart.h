#ifndef __UART_H
#define __UART_H

#include <WString.h>

void uartInit();                // initialize UART
void uartSend(String message);  // send a character over UART
void uartReceive();             // send a string over UART
void onReceiveFunction(void);   // callback function for UART receive
void uartOnReceiveCallbackInit(); // initialize callback function for UART receive

#endif