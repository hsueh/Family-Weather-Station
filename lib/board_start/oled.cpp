//#include <Arduino.h>
#include <U8g2lib.h>

// 构造对象
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, 
    /* clock=*/ 39, /* data=*/ 40,/* reset=*/ U8X8_PIN_NONE); // 开发板无重置引脚

void oledInit() {
    Serial.begin(115200);    // 串口初始化

    u8g2.setBusClock(800000);   // 设置时钟频率  
    u8g2.begin();               // 初始化
    u8g2.enableUTF8Print();     // 开启UTF-8中文支持
    u8g2.setFontDirection(0);   // 设置字体方向

    u8g2.setFont(u8g2_font_wqy16_t_gb2312b);    // 设置字体

    int x = u8g2.getMaxCharHeight();    //获取
    int y = u8g2.getMaxCharWidth();
    Serial.println(x);
    Serial.println(y);
}

void demo()
{
    u8g2.clearBuffer();

    u8g2.setFont(u8g2_font_ncenB12_tr);
    u8g2.drawStr(0, 13, "Hello World!");

    u8g2.setFont(u8g2_font_wqy14_t_gb2312b);    // 换成中文字体
    u8g2.setCursor(0, 30);
    u8g2.print("你不要过来啊！");
    u8g2.drawUTF8(0, 50, "呵呵");

    u8g2.setFont(u8g2_font_open_iconic_weather_4x_t);
    u8g2.drawGlyph(90, 60, 0x0045); // 你

    u8g2.sendBuffer();
    u8g2.clearDisplay();

}
