#ifndef __FULL_COLOR_LED_H_
#define __FULL_COLOR_LED_H_

void LEDInit();

void whiteLED();

void ledOff();

void fullColorLED();

void redLED();

void yellowLED();

void greenLED();

void almLED();

#endif