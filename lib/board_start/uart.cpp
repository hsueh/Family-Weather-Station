#include "HardwareSerial.h"

void uartInit() {
    // Initialize UART
    Serial.begin(115200);
    Serial.println("[ESP32S3] -> uart is ready");
    Serial.flush();

}

void uartSend(String message) {
    // Send message over UART
    Serial.println("[ESP32S3] -> uart send: " + message);
    Serial.flush();
    //delay(100);
}

void uartReceive() {
    // Receive message over UART
    if (Serial.available() > 0) {
        String message = Serial.readStringUntil('\n');
        Serial.println("[ESP32S3] -> onReceive callback: " + message);

    }

}

// 串口0接收中断回调函数
void onReceiveFunction(void) {
    // 接收串口1发送过来的数据长度
    size_t available = Serial.available();

    //显示接收长度
    Serial.printf("[ESP32S3] -> onReceive callback: There are %d bytes available: ", available);
    //接收长度不为0则一直减
    while (available --)
    {
        //通过串口0显示串口1接收到的数据
        Serial.print((char)Serial.read());
    }
    //显示换行
    Serial.println();
}

// 为串口0设置回调函数
void uartOnReceiveCallbackInit() {
    Serial.onReceive(onReceiveFunction);
}
