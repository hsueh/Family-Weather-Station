#ifndef __BUZZER_H
#define __BUZZER_H

void buzzerInit();

void buzzerShort();

void buzzerLong();

void buzzerALM();

#endif