#include <Arduino.h>

// 普通LED 共阴极
// #define LED_R 3
// #define LED_G 2
// #define LED_B 1

// 5050全彩LED 共阴极
// #define LED_R 2
// #define LED_G 1
// #define LED_B 3

// 10mm LED 共阳极
#define LED_R 3
#define LED_G 1
#define LED_B 2

int val;

void LEDInit() {
    pinMode(LED_R, OUTPUT);
    pinMode(LED_G, OUTPUT);
    pinMode(LED_B, OUTPUT);
}

void whiteLED() {
    analogWrite(LED_R, 255);
    analogWrite(LED_G, 255);
    analogWrite(LED_B, 255);

    // 共阳极
    // digitalWrite(LED_R, 0);
    // digitalWrite(LED_G, 0);
    // digitalWrite(LED_B, 0);
}

void ledOff() {
    digitalWrite(LED_R, 0);
    digitalWrite(LED_G, 0);
    digitalWrite(LED_B, 0);

    // 共阳极
    // analogWrite(LED_R, 255);
    // analogWrite(LED_G, 255);
    // analogWrite(LED_B, 255);
}

void fullColorLED() {
    for (int val = 255; val > 0; val--)
    {
        analogWrite(LED_R, val);
        analogWrite(LED_B, 255 - val);
        analogWrite(LED_G, 128 - val);
        delay(10);
    }
    for (int val = 0; val < 255; val++)
    {
        analogWrite(LED_R, val);
        analogWrite(LED_B, 255 - val);
        analogWrite(LED_G, 128 - val);
        delay(10);
    }

}

void redLED() {
    analogWrite(LED_R, 255);
    analogWrite(LED_G, 0);
    analogWrite(LED_B, 0);

    // 共阳极
    // digitalWrite(LED_R, 0);
    // digitalWrite(LED_G, 255);
    // digitalWrite(LED_B, 255);
}

void yellowLED() {
    analogWrite(LED_R, 255);
    analogWrite(LED_G, 255);
    analogWrite(LED_B, 0);

    // 共阳极
    // digitalWrite(LED_R, 0);
    // digitalWrite(LED_G, 0);
    // digitalWrite(LED_B, 255);
}

void greenLED() {
    analogWrite(LED_R, 0);
    analogWrite(LED_G, 255);
    analogWrite(LED_B, 0);

    // 共阳极
    // digitalWrite(LED_R, 255);
    // digitalWrite(LED_G, 0);
    // digitalWrite(LED_B, 255);
}

// LED 警灯
void almLED() { 
    // 共阴极
    analogWrite(LED_R, 255);
    delay(200);
    analogWrite(LED_R, 0);
    
    analogWrite(LED_B, 255);
    delay(200);
    analogWrite(LED_B, 0);

    // 共阳极
    // analogWrite(LED_R, 0);
    // analogWrite(LED_G, 255);
    // analogWrite(LED_B, 255);
    // delay(200);
    // analogWrite(LED_R, 255);
    // analogWrite(LED_G, 255);
    // analogWrite(LED_B, 255);
    
    // analogWrite(LED_R, 255);
    // analogWrite(LED_G, 255);
    // analogWrite(LED_B, 0);
    // delay(200);
    // analogWrite(LED_R, 255);
    // analogWrite(LED_G, 255);
    // analogWrite(LED_B, 255);

}

