#include <Arduino.h>
#define buzzerPin 5

void buzzerInit() {
    pinMode(buzzerPin, OUTPUT);
}

// 短音
void buzzerShort() {
    digitalWrite(buzzerPin, LOW);
    delay(200);
    digitalWrite(buzzerPin, HIGH);
    delay(200);
}

// 长音
void buzzerLong() {
    digitalWrite(buzzerPin, LOW);
    delay(600);
    digitalWrite(buzzerPin, HIGH);
    delay(200);
}

// ALM警报
void buzzerALM() {
    // A
    buzzerShort();
    buzzerLong();

    // L
    buzzerShort();
    buzzerLong();
    buzzerShort();
    buzzerShort();

    // M
    buzzerLong();
    buzzerLong();

}